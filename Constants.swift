//
//  Constants.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import Foundation

/// Constants definitions
struct Constants {

	static let animalServiceURL: String = "http://files.ilicco.com/digitaslbi/recruitment/test.json"
	static let showDetailSegueName: String = "showDetail"

	/// JSON Keys definitions
	struct JSONKeys {
		static let animalsList: String = "Animals"
		static let favouritesList: String = "favourites"
		static let othersList: String = "others"
		static let species: String = "species"
		static let family: String = "family"
		static let IUCNCategory: String = "IUCN"
		static let year: String = "year"
		static let picture: String = "picture"
		static let notes: String = "notes"
		static let notesContent: String = "__cdata"
	}

	/// Error copies
	struct ErrorMessages {
		static let connectivityError: String = "Uh oh! Something went wrong, seems like you don't have an Internet connection right now. Try again later!"
		static let dataError: String = "Uh oh! Something went wrong with our server, our bad! Try again later!"
		static let imageDownloadError: String = "Uh oh! Something went wrong, our bad! Try again later!"
	}

	/// Image names
	struct Icons {

		static let checkImageName: String = "check"
		static let addImageName: String = "add"
	}
}