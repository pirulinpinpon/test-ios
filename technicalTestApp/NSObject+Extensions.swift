//
//  NSObject+Extensions.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 25/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import Foundation

extension NSObject {

	/**
	 Returns the name of the class.

	 - Returns: The `String` value of the class' raw name.

	 */
	public class var className: String {
		return NSStringFromClass(self).componentsSeparatedByString(".").last!
	}

	/**
	 Returns the name of the instance's class.

	 - Returns: The `String` value of the class' raw name.

	 */
	public var className: String {
		return NSStringFromClass(self.dynamicType).componentsSeparatedByString(".").last!
	}
}