//
//  DetailViewController.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

	// MARK: - IBOutlets

	@IBOutlet weak var familyNameTitleLabel: UILabel!
	@IBOutlet weak var familyNameLabel: UILabel!
	@IBOutlet weak var IUCNCategoryTitleLabel: UILabel!
	@IBOutlet weak var IUCNCategoryLabel: UILabel!
	@IBOutlet weak var yearTitleLabel: UILabel!
	@IBOutlet weak var yearLabel: UILabel!

	@IBOutlet var pictureImageView: UIImageView!
	@IBOutlet var notesLabel: UILabel!

	@IBOutlet var imageActivityIndicatorView: UIActivityIndicatorView!

	@IBOutlet var addToFavouritesBarButtonItem: UIBarButtonItem!

	// MARK: - Properties

	var animal: Animal? {
		didSet {
			// Update the view.
			self.configureView()
		}
	}

	// MARK: - UI Setup

	func configureView() {
		// Update the user interface with animal's data
		if let unwrappedAnimal = self.animal {

			self.title = unwrappedAnimal.species

			if let _ = self.familyNameLabel {
				self.familyNameLabel.text = unwrappedAnimal.family
			}

			if let _ = self.IUCNCategoryLabel {
				self.IUCNCategoryLabel.text = unwrappedAnimal.IUCN
			}

			if let _ = self.yearLabel {
				self.yearLabel.text = unwrappedAnimal.year
			}

			if let _ = self.notesLabel {
				self.notesLabel.text = unwrappedAnimal.notes
			}

			if let _ = self.pictureImageView {

				if let picture = unwrappedAnimal.picture.image {

					self.pictureImageView.image = picture
					self.imageActivityIndicatorView.hidden = true
				} else {

					self.downloadPicture(unwrappedAnimal.picture.imageURL)
				}
			}

			if let _ = self.addToFavouritesBarButtonItem {

				self.addToFavouritesBarButtonItem.target = self
				self.addToFavouritesBarButtonItem.action = #selector(addToFavourites)

				if unwrappedAnimal.isFavourite {

					self.addToFavouritesBarButtonItem.image = UIImage(named: Constants.Icons.checkImageName)
				} else {
					self.addToFavouritesBarButtonItem.image = UIImage(named: Constants.Icons.addImageName)
				}
			}
		}
	}

	// MARK: - Lifecycle methods

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		self.configureView()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// MARK: - Actions

	func addToFavourites() {

		if let unwrappedAnimal = self.animal {

			unwrappedAnimal.isFavourite = !unwrappedAnimal.isFavourite

			if unwrappedAnimal.isFavourite {

				self.addToFavouritesBarButtonItem.image = UIImage(named: Constants.Icons.checkImageName)
			} else {

				self.addToFavouritesBarButtonItem.image = UIImage(named: Constants.Icons.addImageName)
			}
		}
	}

	// MARK: - Helper callers

	func downloadPicture(url: String) {

		if let pictureURL = NSURL(string: url) {

			NetworkingHelper.downloadImage(pictureURL, completionHandler: { (image) in

				self.animal?.picture.image = image
				self.pictureImageView.image = image
				self.imageActivityIndicatorView.stopAnimating()
				self.imageActivityIndicatorView.hidden = true
			})
		}
	}
}
