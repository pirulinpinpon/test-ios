//
//  MasterViewController.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

	var detailViewController: DetailViewController? = nil
	var animals: [Animal]?

	let cellHeight = CGFloat(110)

	override func viewDidLoad() {

		super.viewDidLoad()

		self.tableView.registerNib(UINib(nibName: AnimalTableViewCell.className, bundle: nil), forCellReuseIdentifier: AnimalTableViewCell.className)

		if let split = self.splitViewController {
			let controllers = split.viewControllers
			self.detailViewController = (controllers[controllers.count - 1] as! UINavigationController).topViewController as? DetailViewController
		}

		self.fetchAnimals()
	}

	override func viewWillAppear(animated: Bool) {

		self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
		super.viewWillAppear(animated)
	}

	override func didReceiveMemoryWarning() {

		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}

	// MARK: - Segues

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {

		if segue.identifier == Constants.showDetailSegueName {

			if let indexPath = self.tableView.indexPathForSelectedRow, let unwrappedAnimals = self.animals {

				let animal = unwrappedAnimals[indexPath.row]
				let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
				controller.animal = animal
				controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
				controller.navigationItem.leftItemsSupplementBackButton = true
			}
		}
	}

	// MARK: - Table View Data Source

	override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if let unwrappedAnimals = self.animals where unwrappedAnimals.count > 0 {
			return unwrappedAnimals.count
		} else {
			return 1
		}
	}

	override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

		if let unwrappedAnimals = self.animals {

			if unwrappedAnimals.count > 0 {

				let cell = tableView.dequeueReusableCellWithIdentifier(AnimalTableViewCell.className, forIndexPath: indexPath)

				if let animalCell = cell as? AnimalTableViewCell {

					let animal = unwrappedAnimals[indexPath.row] // !!!
					animalCell.animal = animal

					return animalCell
				} else {

					return cell
				}
			} else {

				let frame = CGRectMake(0, 0, tableView.contentSize.width, cellHeight)
				let cell = UITableViewCell(frame: frame)
				let errorView = ErrorView(frame: frame)
				errorView.errorMessageLabel.text = Constants.ErrorMessages.dataError
				errorView.reloadButton.addTarget(self, action: #selector(fetchAnimals), forControlEvents: .TouchUpInside)
				cell.contentView.addSubview(errorView)

				return cell
			}
		} else {

			let frame = CGRectMake(0, 0, tableView.contentSize.width, cellHeight)
			let cell = UITableViewCell(frame: frame)
			let errorView = ErrorView(frame: frame)
			errorView.errorMessageLabel.text = Constants.ErrorMessages.connectivityError
			errorView.reloadButton.addTarget(self, action: #selector(fetchAnimals), forControlEvents: .TouchUpInside)
			cell.contentView.addSubview(errorView)

			return cell
		}
	}

	// MARK: - Table View Delegate

	override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

		return cellHeight - 1
	}

	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

		if let _ = self.animals {

			self.performSegueWithIdentifier(Constants.showDetailSegueName, sender: tableView)
		} else {

			self.fetchAnimals()
		}
	}

	// MARK: - Helper callers

	func fetchAnimals() {

		AnimalHelper.fetchAnimals({ (animals) in

			self.animals = animals
			self.tableView.reloadData()
			},
			failure: { (error) in

				switch error {

				case .InvalidURL, .RequestFailure:

					self.animals = nil
					break

				default:

					self.animals = [Animal]()
					break
				}
		})
	}
}
