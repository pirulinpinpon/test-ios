//
//  AnimalParser.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import Foundation

class AnimalParser: NSObject {

	/**
	 Parses received data into a collection of animals, i.e.: `[Animal]`.

	 - Parameter data: JSON's data.
	 - Parameter success: closure for handling success on parsing process, receives the collection of animals.
	 - Parameter failure: closure for handling failure on parsing process, receives an error.
	 */
	func parse(data: NSData, success: (animals: [Animal]?) -> Void, failure: (error: NSError) -> Void) {

		var result: [Animal]?

		do {

			let JSONObject: AnyObject? = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments)

			if let unwrappedJSONObject = JSONObject, JSONDictionary = unwrappedJSONObject as? NSDictionary {

				if let animalsDictionary = JSONDictionary[Constants.JSONKeys.animalsList] as? NSDictionary {

					if let favourites = animalsDictionary[Constants.JSONKeys.favouritesList] as? NSArray {

						if let animals = parseAnimalCollection(favourites, isFavourite: true) {

							if let _ = result {
								result?.appendContentsOf(animals)
							} else {
								result = animals
							}
						}
					}

					if let others: NSArray = animalsDictionary[Constants.JSONKeys.othersList] as? NSArray {

						if let animals = parseAnimalCollection(others, isFavourite: false) {

							if let _ = result {
								result?.appendContentsOf(animals)
							} else {
								result = animals
							}
						}
					}
				}
			}

			success(animals: result)
		} catch(let error as NSError) {

			failure(error: error)
		}
	}

	/**
	 Parses a `NSDictionary` with an animal data into a new instance of `Animal`.

	 - Parameter animalDictionary: animal's data.
	 - Parameter isFavourite: `true` is animal is classified as **favourite**, `false` if classified as **other**.

	 - Returns: A new `Animal` instance.
	 */
	func parseAnimal(animalDictionary: NSDictionary, isFavourite: Bool) -> Animal? {

		guard let species = animalDictionary[Constants.JSONKeys.species] as? String,
			let family = animalDictionary[Constants.JSONKeys.family] as? String,
			let IUCN = animalDictionary[Constants.JSONKeys.IUCNCategory] as? String,
			let year = animalDictionary[Constants.JSONKeys.year] as? String,
			let picture = animalDictionary[Constants.JSONKeys.picture] as? String,
			let notes = animalDictionary[Constants.JSONKeys.notes] as? NSDictionary,
			let notesContent = notes[Constants.JSONKeys.notesContent] as? String else {

				return nil
		}

		let animal = Animal(species: species,
			family: family,
			IUCN: IUCN,
			year: year,
			picture: AnimalPicture(imageURL: picture),
			notes: notesContent,
			isFavourite: isFavourite)

		return animal
	}

	// MARK: Helper methods

	/// Loop through JSON collection
	func parseAnimalCollection(animalCollection: NSArray, isFavourite: Bool) -> [Animal]? {

		var result: [Animal]?

		for animalDictionary in animalCollection {

			if let unwrappedAnimalDictionary = animalDictionary as? NSDictionary {

				if let animal = self.parseAnimal(unwrappedAnimalDictionary, isFavourite: isFavourite) {

					if let _ = result {
						result?.append(animal)
					} else {
						result = [animal]
					}
				}
			}
		}

		return result
	}
}