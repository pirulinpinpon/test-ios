//
//  AnimalHelper.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import Foundation

// MARK: - Type alias for handlers
typealias AnimalHelperCompletionHandler = ((animals: [Animal]) -> Void)
typealias AnimalHelperFailureHandler = ((error: AnimalHelperErrorType) -> Void)

// MARK: - Error's type for requests handled by `AnimalHelper`
enum AnimalHelperErrorType: ErrorType {

	case InvalidURL
	case RequestFailure
	case InvalidData
	case Undefined
}

class AnimalHelper: NSObject {

	/**
	 Requests animal's data from server.

	 - Parameter completion:
	 - Parameter failure:
	 */
	class func fetchAnimals(completion: AnimalHelperCompletionHandler, failure: AnimalHelperFailureHandler) {

		if let URL = NSURL(string: Constants.animalServiceURL) {

			// Request data
			NetworkingHelper.getDataFromUrl(URL, completion: { (data, response, error) in

				guard let unwrappedData = data where error == nil else {
					// Request failure
					failure(error: .RequestFailure)
					return
				}

				// Parse response
				AnimalParser().parse(unwrappedData,
					success: { (animals) in

						if let unwrappedAnimals = animals {
							completion(animals: unwrappedAnimals)
						} else {
							failure(error: .InvalidData)
						}
					},
					failure: { (error) in
						failure(error: .InvalidData)
				})
			})
		} else {

			failure(error: .InvalidURL)
		}
	}
}
