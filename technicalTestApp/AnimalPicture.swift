//
//  AnimalPicture.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import UIKit

class AnimalPicture: NSObject {

	var image: UIImage?
	var imageURL: String = ""

	// MARK: - Initilizer

	init(imageURL: String) {

		self.imageURL = imageURL
		super.init()
	}
}
