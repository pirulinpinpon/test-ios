//
//  UIView+Extensions.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 25/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import Foundation

import UIKit

extension UIView {

	/**
	 Loads the view from the corresponding NIB.

	 It's important the the NIB name is equal to class' name.
	 */
	public func loadViewFromNib() {

		let bundle = NSBundle(forClass: self.dynamicType)
		let nib = UINib(nibName: self.className, bundle: bundle)
		let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
		view.frame = bounds
		view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
		self.addSubview(view);
	}
}