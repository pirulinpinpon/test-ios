//
//  AnimalTableViewCell.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import UIKit

class AnimalTableViewCell: UITableViewCell {

	// MARK: IBOutlets

	@IBOutlet var animalSpeciesNameLabel: UILabel!
	@IBOutlet var animalFamilyNameLabel: UILabel!
	@IBOutlet var IUCNCategoryNameLabel: UILabel!
	@IBOutlet var yearLabel: UILabel!
	@IBOutlet var animalPictureImageView: UIImageView!
	@IBOutlet var imageActivityIndicatorView: UIActivityIndicatorView!

	// MARK: - Properties

	var animal: Animal? {
		didSet {
			// Update the view.
			self.configureView()
		}
	}

	// MARK: - Lifecycle

	override func awakeFromNib() {

		super.awakeFromNib()
		self.imageActivityIndicatorView.startAnimating()
	}

	// MARK: - UI Setup

	func configureView() {

		if let unwrappedAnimal = self.animal {

			self.animalFamilyNameLabel.text = unwrappedAnimal.family
			self.animalSpeciesNameLabel.text = unwrappedAnimal.species
			self.IUCNCategoryNameLabel.text = unwrappedAnimal.IUCN
			self.yearLabel.text = unwrappedAnimal.year

			if let picture = unwrappedAnimal.picture.image {

				self.animalPictureImageView.image = picture
				self.imageActivityIndicatorView.hidden = true
			} else {

				self.downloadPicture(unwrappedAnimal.picture.imageURL)
			}
		}
	}

	// MARK: - Helper callers

	func downloadPicture(url: String) {

		if let pictureURL = NSURL(string: url) {

			NetworkingHelper.downloadImage(pictureURL, completionHandler: { (image) in

				self.animal?.picture.image = image
				self.animalPictureImageView.image = image
				self.imageActivityIndicatorView.stopAnimating()
				self.imageActivityIndicatorView.hidden = true
			})
		}
	}
}
