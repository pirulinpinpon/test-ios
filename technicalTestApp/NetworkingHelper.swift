//
//  NetworkingHelper.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import UIKit

typealias RequestCompletionHandler = ((data: NSData?, response: NSURLResponse?, error: NSError?) -> Void)
typealias ImageDownloadCompletionHandler = ((image: UIImage?) -> Void)

class NetworkingHelper: NSObject {

	/**
	 Downloads data for image from the specified URL if available.

	 - Parameter url: URL for image.
	 - Parameter completionHandler: closure for handling image download completion, receives the resulting `UIImage` if available.
	 */
	class func downloadImage(url: NSURL, completionHandler: ImageDownloadCompletionHandler) {

		getDataFromUrl(url, completion: { (data, response, error) in

			dispatch_async(dispatch_get_main_queue()) { () -> Void in

				guard let unwrappedData = data where error == nil else {
					completionHandler(image: nil)
					return
				}

				let image = UIImage(data: unwrappedData)
				completionHandler(image: image)
			}
		})
	}

	/**
	 Requests data from the specified URL if available.

	 - Parameter url: URL to request to.
	 - Parameter completionHandler: closure for handling request completion, receives the obtained data, response or error when the request fails.
	 */
	class func getDataFromUrl(url: NSURL, completion: RequestCompletionHandler) {

		NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in

			completion(data: data, response: response, error: error)
		}.resume()
	}
}
