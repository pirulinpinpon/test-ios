//
//  Animal.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 20/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import Foundation

/// Class for an Animal
class Animal: NSObject {

	/// Biological species name
	let species: String
	/// Biological family name
	let family: String
	/// IUCN's category
	let IUCN: String
	/// Year
	let year: String
	/// Picture
	let picture: AnimalPicture
	/// Description notes
	let notes: String
	/// Favourite flag
	var isFavourite: Bool

	// MARK: - Initilizer

	init(species: String, family: String, IUCN: String, year: String, picture: AnimalPicture, notes: String, isFavourite: Bool) {

		self.species = species
		self.family = family
		self.IUCN = IUCN
		self.year = year
		self.picture = picture
		self.notes = notes
		self.isFavourite = isFavourite

		super.init()
	}
}
