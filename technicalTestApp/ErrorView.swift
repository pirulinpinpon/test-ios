//
//  ErrorView.swift
//  technicalTestApp
//
//  Created by Jerilyn Gonçalves on 25/05/16.
//  Copyright © 2016 Jerilyn Gonçalves. All rights reserved.
//

import UIKit

class ErrorView: UIView {

	@IBOutlet var errorMessageLabel: UILabel!
	@IBOutlet var reloadButton: UIButton!

	// MARK: - Initializers

	override init(frame: CGRect) {

		super.init(frame: frame)
		loadViewFromNib()
	}

	required init?(coder aDecoder: NSCoder) {

		super.init(coder: aDecoder)
		loadViewFromNib()
	}
}
